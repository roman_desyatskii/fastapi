Electronic Journal
---
### Описание проекта

* Приложение представляет собой упрощенную версию электронного журнала для оценок.

### Требования

- Python 3.10+
- PostgreSQL
- Git

### Инструкция по настройке проекта:
1. Создать или открыть папку куда будет копироваться проект
2. Открыть папку и прописать команду git clone https://gitlab.com/roman_desyatskii/fastapi.git
3. Перейти в появившуюся папку, и создать виртуальное окружение прописав в терминале команду python3 -m venv venv
4. Активировать виртуальное окружение . venv/bin/activate
5. Установите зависимости командой pip install -r requirements.txt
6. Создайте базу данных. [postgesql создание Базы данных](https://postgrespro.ru/docs/postgresql/9.5/manage-ag-createdb)
Команда создание базы данных в консоле DataGrib или PgAdmin CREATE DATABASE test
7. Создайте файл .env в корне проекта и добавьте следующие переменные окружения:
```
   DB_HOST=localhost
   DB_PORT=your_port
   DB_NAME=your_database_name
   DB_USER=your_db_user
   DB_PASS=your_db_password 
```
8. Делаем миграцию командой alembic revision --autogenerate -m "Initial migrations"
9. В папке alembic есть папка versions, зайдите в появившейся файл и в строке revision
скопируйте уникальный индентификатор. Пример индентификатора: ('53fd3f2a1c49')
10. В Терминале пропишите команду alembic upgrade ... вставьте вместо троеточия уникальный индентификатор, который
11. Вы раннее скопировали
11. Командой python3 main.py запускаем программу. 

### Тестирование API: 
Вы можете протестировать API с помощью Swagger документации, доступной по адресу http://localhost:8000/docs

## Swagger 
Swagger — это инструмент для документирования и тестирования API, позволяющий автоматически создавать документацию API
из описания структуры API в формате YAML или JSON файла.

[Ссылка по Swagger](https://blog.ithillel.ua/ru/articles/api-testing-with-swagger)
