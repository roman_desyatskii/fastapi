from sqlalchemy import Column, ForeignKey, Integer, String, Table, MetaData

metadata = MetaData()

students = Table(
    "students",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String(255), nullable=False),
    Column("email", String(255), unique=True, nullable=False),
)

scores = Table(
    "scores",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("score", Integer),
    Column("student_id", ForeignKey("students.id")),
)

