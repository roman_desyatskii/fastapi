from fastapi import HTTPException
from sqlalchemy import insert, delete, update
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from src.app.db import schemas
from src.app.db.models.models import students, scores


#CREATE

def create_student(db: Session, student: schemas.StudentCreate):
    query = insert(students).values(name=student.name, email=student.email)
    result = db.execute(query)
    db.commit()
    inserted_id = result.inserted_primary_key[0]
    db_student = db.execute(students.select().where(students.c.id == inserted_id)).fetchone()
    return db_student


def create_score(db: Session, score: schemas.ScoreCreate):
    if not score.student_id:
        raise ValueError("student_id is required to create a score")

    try:
        query = insert(scores).values(student_id=score.student_id, score=score.score).returning(scores.c.id, scores.c.student_id, scores.c.score)
        result = db.execute(query)
        db.commit()
        new_score = result.fetchone()
        return {"id": new_score.id, "student_id": new_score.student_id, "score": new_score.score}
    except IntegrityError:
        db.rollback()
        raise HTTPException(status_code=404, detail="Student not found")

#READE


def get_student(db: Session, student_id: int):
    query = students.select().where(students.c.id == student_id)
    return db.execute(query).fetchone()


def get_student_by_email(db: Session, email: str):
    query = students.select().where(students.c.email == email)
    return db.execute(query).fetchone()


def get_students(db: Session, skip: int = 0, limit: int = 10):
    query = students.select().offset(skip).limit(limit)
    return db.execute(query).fetchall()


def get_score(db: Session, score_id: int):
    query = scores.select().where(scores.c.id == score_id)
    result = db.execute(query).fetchone()

    if result:
        return {"id": result.id, "student_id": result.student_id, "score": result.score}
    return None


def get_scores(db: Session, skip: int = 0, limit: int = 10):
    query = scores.select().offset(skip).limit(limit)
    return db.execute(query).fetchall()


#DELETE

def delete_student(db: Session, student_id: int):
    student = db.query(students).filter(students.c.id == student_id).first()
    if student is None:
        raise HTTPException(status_code=404, detail="Student not found")

    try:
        db.execute(delete(scores).where(scores.c.student_id == student_id))
        db.execute(delete(students).where(students.c.id == student_id))
        db.commit()
    except Exception as e:
        db.rollback()
        raise HTTPException(status_code=500, detail="Failed to delete student")

    return {"message": "Student deleted successfully"}


def delete_score(db: Session, score_id: int):
    query = scores.delete().where(scores.c.id == score_id)
    result = db.execute(query)
    db.commit()
    if result.rowcount == 0:
        return None
    return {"message": "Score deleted"}


#UPDATE
def update_student(db: Session, student_id: int, student_data: dict) -> schemas.Student:
    query = (
        update(students)
        .where(students.c.id == student_id)
        .values(**student_data)
        .returning(students)
    )
    result = db.execute(query)
    db.commit()
    updated_student = result.fetchone()
    return updated_student


def update_score(db: Session, score_id: int, score_data: dict):
    student_id = score_data.get("student_id")
    if student_id is not None:
        student = db.query(students).filter(students.c.id == student_id).first()
        if student is None:
            raise HTTPException(status_code=404, detail="Student not found")

    query = (
        scores.update()
        .where(scores.c.id == score_id)
        .values(**score_data)
        .returning(*scores.columns)
    )
    updated_scores = db.execute(query).fetchall()
    db.commit()

    if not updated_scores:
        return None

    updated_score = updated_scores[0]

    return schemas.Score(id=updated_score.id, student_id=updated_score.student_id, score=updated_score.score)
