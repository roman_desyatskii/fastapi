from typing import Optional

from pydantic import BaseModel


class StudentSchema(BaseModel):
    name: str
    email: str


class StudentCreate(StudentSchema):
    pass


class Student(StudentSchema):
    id: int

    class Config:
        orm_mode = True


class StudentUpdate(StudentSchema):
    name: Optional[str] = None
    email: Optional[str] = None


class ScoreSchema(BaseModel):
    student_id: int
    score: int


class ScoreCreate(ScoreSchema):
    pass


class Score(ScoreSchema):
    id: int

    class Config:
        orm_mode = True


class ScoreUpdate(ScoreSchema):
    student_id: Optional[int]
    score: Optional[int]

    class Config:
        schema_extra = {
            "example": {
                "student_id": 5,
                "score": 90
            }
        }