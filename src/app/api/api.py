import fastapi

from src.app.db import schemas
from src.app.db import repositories
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from src.app.core.settings import SessionLocal

router = fastapi.APIRouter(prefix="/api")

app = fastapi.FastAPI()
app.include_router(router)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


#Main


@app.get("/", tags=["Main"])
def read_index():
    return "Hello"


#Students

@app.get("/students/", response_model=list[schemas.Student], tags=["Students"])
def read_students(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    students = repositories.get_students(db, skip=skip, limit=limit)
    return students


@app.get("/students/{student_id}", response_model=schemas.Student, tags=["Students"])
def read_user(student_id: int, db: Session = Depends(get_db)):
    db_student = repositories.get_student(db, student_id=student_id)
    if db_student is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_student


@app.post("/students/", response_model=schemas.Student, tags=["Students"])
def create_student(student: schemas.StudentCreate, db: Session = Depends(get_db)):
    db_user = repositories.get_student_by_email(db, email=student.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")

    created_student = repositories.create_student(db=db, student=student)
    return created_student


@app.delete("/students/{student_id}", tags=["Students"])
def delete_student(student_id: int, db: Session = Depends(get_db)):
    return repositories.delete_student(db=db, student_id=student_id)


@app.patch("/students/{student_id}", response_model=schemas.Student, tags=["Students"])
def update_student(student_id: int, student: schemas.StudentUpdate, db: Session = Depends(get_db)):
    student_data = student.dict(exclude_unset=True)
    db_student = repositories.update_student(db=db, student_id=student_id, student_data=student_data)
    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found")
    return db_student


#Scores


@app.get("/scores/{score_id}", response_model=schemas.Score, tags=["Scores"])
def read_score(score_id: int, db: Session = Depends(get_db)):
    db_score = repositories.get_score(db=db, score_id=score_id)
    if db_score is None:
        raise HTTPException(status_code=404, detail="Score not found")
    return db_score


@app.post("/scores/", response_model=schemas.Score, tags=["Scores"])
def create_score(score: schemas.ScoreCreate, db: Session = Depends(get_db)):
    return repositories.create_score(db=db, score=score)


@app.get("/scores/", response_model=list[schemas.Score], tags=["Scores"])
def read_scores(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    scores = repositories.get_scores(db, skip=skip, limit=limit)
    return scores


@app.delete("/scores/{score_id}", tags=["Scores"])
def delete_score(score_id: int, db: Session = Depends(get_db)):
    result = repositories.delete_score(db=db, score_id=score_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Score not found")
    return result


@app.patch("/scores/{score_id}", response_model=schemas.Score, tags=["Scores"])
def update_score(score_id: int, score: schemas.ScoreUpdate, db: Session = Depends(get_db)):
    score_data = score.dict(exclude_unset=True)
    db_score = repositories.update_score(db=db, score_id=score_id, score_data=score_data)
    if db_score is None:
        raise HTTPException(status_code=404, detail="Score not found")
    return db_score
